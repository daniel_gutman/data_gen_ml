# keras module for building LSTM

from keras.preprocessing.sequence import pad_sequences
from keras.layers import Embedding, LSTM, Dense, Dropout
from keras.preprocessing.text import Tokenizer
from keras.models import Sequential
from keras.utils import multi_gpu_model
import keras.utils as ku
import os
# set seeds for reproducability
from tensorflow import set_random_seed
from tensorflow import logging
import tensorflow as tf
from numpy.random import seed
import numpy as np
import random
import warnings


set_random_seed(2)
seed(1)
warnings.filterwarnings("ignore")
warnings.simplefilter(action='ignore', category=FutureWarning)

os.environ['TF_CPP_MIN_VLOG_LEVEL'] = '3'
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

logging.set_verbosity(logging.INFO)

gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.92)
sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options))

class gen_data(object):
    def __init__(self, data, EPOCH, GPU):

        self.corpus = data

        self.tokenizer = Tokenizer()

        self.inp_sequences, self.total_words = get_sequence_of_tokens(self)

        self.predictors, self.label, self.max_sequence_len = generate_padded_sequences(self.inp_sequences,self)

        self.model = create_model(self.max_sequence_len, self.total_words)
        self.model.summary()

        if GPU >= 2:
            self.parallel_model = multi_gpu_model(self.model, gpus=GPU)
            self.parallel_model.compile(loss='categorical_crossentropy', optimizer='rmsprop')

            self.parallel_model.fit(self.predictors, self.label, epochs=EPOCH, verbose=1)

        else:
            self.model.fit(self.predictors, self.label, epochs=EPOCH, verbose=1)

        self.new_data = generate_text(random.choice(self.corpus), 5, self.model, self.max_sequence_len,self)

def get_sequence_of_tokens(self):
    ## tokenization
    self.tokenizer.fit_on_texts(self.corpus)
    total_words = len(self.tokenizer.word_index) + 1

    ## convert data to sequence of tokens
    input_sequences = []
    for line in self.corpus:
        token_list = self.tokenizer.texts_to_sequences([line])[0]
        for i in range(1, len(token_list)):
            n_gram_sequence = token_list[:i + 1]
            input_sequences.append(n_gram_sequence)
    return input_sequences, total_words



def generate_padded_sequences(input_sequences,self):
    max_sequence_len = max([len(x) for x in input_sequences])
    input_sequences = np.array(pad_sequences(input_sequences, maxlen=max_sequence_len, padding='pre'))

    predictors, label = input_sequences[:, :-1], input_sequences[:, -1]
    label = ku.to_categorical(label, num_classes=self.total_words)
    return predictors, label, max_sequence_len




def create_model(max_sequence_len, total_words):
    input_len = max_sequence_len - 1
    model = Sequential()

    # Add Input Embedding Layer
    model.add(Embedding(total_words, 10, input_length=input_len))

    # Add Hidden Layer 1 - LSTM Layer
    model.add(LSTM(100))
    model.add(Dropout(0.1))

    # Add Output Layer
    model.add(Dense(total_words, activation='softmax'))

    model.compile(loss='categorical_crossentropy', optimizer='adam')

    return model


def generate_text(seed_text, next_words, model, max_sequence_len,self):
    for _ in range(next_words):
        token_list = self.tokenizer.texts_to_sequences([seed_text])[0]
        token_list = pad_sequences([token_list], maxlen=max_sequence_len - 1, padding='pre')
        predicted = model.predict_classes(token_list, verbose=0)

        output_word = ""
        for word, index in self.tokenizer.word_index.items():
            if index == predicted:
                output_word = word
                break
        seed_text += " " + output_word
    return seed_text.title()
