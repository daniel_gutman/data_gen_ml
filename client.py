#!/usr/bin/env python3

import arg_parser as ap
from gen import *
from utils import *
import gen_devops as gd
import os.path
import analyze_data as ad
import load
import numpy


###TODO
#   4. implement graph of origin data with new data - (https://towardsdatascience.com/the-next-level-of-data-visualization-in-python-dd6e99039d5e)



def add_file():
    csv_file = rinput("Enter csv file with headers:")

    if not os.path.isfile(csv_file):
        e = "File '" + csv_file + "' does not exist: '" + csv_file + "'"
        raise Exception(e)

    return csv_file


def do(flags):

    if "epoch" in flags:
        EPOCH = int(flags['epoch'][0])
    else:
        EPOCH = 5

    if "chunk_size" in flags:
        CHUNK = int(flags['chunk_size'][0])
    else:
        CHUNK = 50

    if "gpu" in flags:
        GPU = int(flags['gpu'][0])
    else:
        GPU = 1

    if "gen" in flags:
        csv_file = add_file()
        delimiter = checks_delimiter(flags)
        error_bad_line = checks_bad_line_status(flags)
        df_orig = get_data(csv_file, delimiter, error_bad_line)

        str_columns_to_print = convet_list_to_string(df_orig.dtypes.to_string().split('\n'), ',').replace('\n', '')

        str_columns_to_choose = convet_list_to_string(list(df_orig.columns.values), ',').replace('\n', '')

        gd.print_columns(str_columns_to_print)
        optional_columns = gd.get_optional_columns(str_columns_to_choose)
        id_column = gd.ask_for_identity_columns(optional_columns)
        filters, target = gd.ask_columns(optional_columns, "filtering", df_orig)

        if id_column:
            max_id = gd.get_id_max_value(id_column, df_orig)
            target = gd.remove_id_from_target(target, id_column)
        else:
            max_id = 0

        learn, random_choose = gd.ask_for_learn_on(df_orig, target)

        size_count = gd.ask_for_file_size()
        save_file = gd.ask_for_output_file()

        genarate_data(save_file, size_count, filters, df_orig, id_column, learn, random_choose, max_id,
                      delimiter, CHUNK, EPOCH, GPU, file_size=0)

    if "describe data" in flags:
        ad.describe(flags)

    if "insert" in flags:
        load.insert_to_SQream(flags)

    if "get" in flags:
        ad.get_max_sting_object_value(flags)


def create_manager_parser():
    parser = ap.create_parser()
    ap.add_argument_to_parser(parser, "gen", "GENERATE DATA")
    ap.add_argument_to_parser(parser, "epoch", "SET EPOCH , DEFAULT IS 5 ", value="Choose number", nargs=1)
    ap.add_argument_to_parser(parser, "gpu", "SET GPU , DEFAULT IS 1 ", value="Choose number", nargs=1)
    ap.add_argument_to_parser(parser, "delimiter", "DELIMITER")
    ap.add_argument_to_parser(parser, "describe data", "DESCRIBE DATA", value="Add two file to compare", file_type=True)
    ap.add_argument_to_parser(parser, "error", "ERROR BAD LINE")
    ap.add_argument_to_parser(parser, "chunk_size", "SET CHUNK SIZE, DEFAULT IS 50", value="Choose number", nargs=1)
    ap.add_argument_to_parser(parser, "Insert_to_SQream",
                              "<sqreamd path, client path, port, gpu, cluster, licence, config,"
                              "database, username, password,ddl, table_name, csv_file, error_log>",
                              value="", nargs=14)
    ap.add_argument_to_parser(parser, "get", "GET MAX STRING OBJECT VALUE IN CSV", value="Add file",
                              file_type=True, nargs=1)

    return ap.parser_parse_args(parser)


if __name__ == "__main__":
    flags = create_manager_parser()
    do(flags)
