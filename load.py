#!/usr/bin/env python3

from gen_building_box import *
import time
import gen_devops as gd

def insert_to_SQream(flags):
        sqreamd_path = flags["insert"][0]
        client_path = flags["insert"][1]
        port = flags["insert"][2]
        gpu = flags["insert"][3]
        cluster = flags["insert"][4]
        lic = flags["insert"][5]
        config = flags["insert"][6]
        database = flags["insert"][7]
        username = flags["insert"][8]
        password = flags["insert"][9]
        ddl = flags["insert"][10]
        table_name = flags["insert"][11]
        csv_file = flags["insert"][12]
        error_log = flags["insert"][13]

        sqreamd_comm = start_sqreamd(sqreamd_path, port, gpu, cluster, lic, config)
        gd.check_port_occupied("sqreamd", port)
        if not sqreamd_comm.is_alive():
            raise Exception(sqreamd_comm.stderr)

        print("Create DDL")
        result, error, code = client_command(client_path, database, username, password, port, ddl, True)
        if code:
            raise Exception(error)
        else:
            print(result)

        cmd = "copy {} from '{}' with ERROR_LOG '{}'".format(table_name, csv_file, error_log)

        print("Insert to SQream")
        result, error, code = client_command(client_path, database, username, password, port, cmd, False)
        if code:
            raise Exception(error)
        else:
            print(result)

        gd.kill_sqream_server("sqreamd", port)
