#!/usr/bin/env python3

from utils import *
from tabulate import tabulate
import sys
import numpy as np
import random
import subprocess
from threading import Thread
import time

def kill_sqream_server(p_name ,port):
    sqream_server_alive = True
    print("Stop sqream server")
    while sqream_server_alive:
        system3("killall -9 sqreamd")
        time.sleep(3)
        sqream_server_alive = check_port_occupied(p_name ,port)


class SubprocessCommunicator(Thread):
    def __init__(self, p):
        Thread.__init__(self)
        self.p = p
        self.stdout = None
        self.stderr = None
        self.code = None

    def is_alive(self):
        return self.code is None

def comm_subprocess(p):
    comm = SubprocessCommunicator(p)
    comm.start()
    return comm


def start_bash_subprocess(command,catch_stds=False):

    if not catch_stds:
        p = subprocess.Popen(['bash', '-c', command])
    else:
        p = subprocess.Popen(['bash', '-c', command],
                             stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE)
    return p


def check_port_occupied(p_name, port, loop=30, sleep=1):
# NOTE: /usr/sbin is not in crontab $PATH! This is why 'lsof' is called with its absolute path!
    cmd = "/usr/sbin/lsof +c 15 -Pi :{} -sTCP:LISTEN | grep {}".format(port,p_name)
    for _ in range(loop):
        res,_,code = system3(cmd)
        if not code:
            if res.decode("utf-8").split(" ")[0] == p_name:
                return True
        time.sleep(sleep)
    return False


def do_command(command, catch_streams=True):
    p = start_bash_subprocess(command, catch_streams)
    result, error = p.communicate()
    code = p.wait()

    return result, error, code

def get_optional_columns(str_columns_to_choose):
    optional_columns = dict(enumerate(str_columns_to_choose.split(','), start=1))
    optional_columns[0] = "Choose more from one"
    del optional_columns[0]
    return optional_columns

def print_columns(str_columns_to_print):
    optional_columns = dict(enumerate(str_columns_to_print.split(','), start=1))
    optional_columns[0] = "Choose more from one"

    headers = ['Columns', "Id"]
    data = sorted([(v, k) for k, v in optional_columns.items()])  # flip the code and name and sort
    data.sort(key=lambda tup: tup[1])
    print(tabulate(data, headers=headers, tablefmt="grid"))
    # del optional_columns[0]
    # return optional_columns


#returns the argument given to the script via command line
def get_argv():
    return sys.argv



#stops the script and prints a comment why
def stop_script(reason):
    print(reason)
    print("Script terminated")
    sys.exit()


def verifiy_object_data(df, learn):

    status = []

    for col in df:
        if col in learn:
            if df[col].dtype == np.object:
                status.append(True)
            else:
                status.append(False)

    return all(status)

def ask_for_learn_on(df, target):
    str_columns_to_print = convet_list_to_string(df[target].dtypes.to_string().split('\n'), ',').replace('\n', '')
    print_columns(str_columns_to_print)

    str_columns = convet_list_to_string(target,',').replace('\n', '')
    optional_columns = get_optional_columns(str_columns)


    learn , random = ask_columns(optional_columns,"training", df)

    return learn , random

def ask_columns(optional_columns, type, df):

    columns_to_filter_on = input("Choose column ids for {}:\n".format(type))

    while (int(columns_to_filter_on) not in optional_columns.keys()) and (int(columns_to_filter_on) != 0):
        columns_to_filter_on = input("Choose a valid ID!!\n")

    if len(columns_to_filter_on) != 1:
        columns_to_filter_on = input("Choose one column or choose 0 for many columns:\n")

    if int(columns_to_filter_on) == 0:
        columns_to_filter_on = input("Please write all id's you want to {} on separated by ',':\n".format(type))
        e = columns_to_filter_on.replace(' ', '').strip()

        while len(e) < 1:
            e = input("Please choose a valid ID!!\n").replace(' ', '').strip()

        columns_list = convert_string_to_list(e,",")

        features_columns = get_choosen_columns(columns_list, optional_columns)

        if type == "learn":
            object_data_status = verifiy_object_data(df, features_columns)
            while object_data_status is False:
                print("Sorry you don't choose a String object,\nSupport only on String object")
                ask_columns(optional_columns, type, df)
                break

        target_columns = get_columns_to_gen(columns_list, optional_columns)

        return features_columns , target_columns
    else:
        features_columns = convert_string_to_list(optional_columns[int(columns_to_filter_on)],",")

        if type == "learn":
            object_data_status = verifiy_object_data(df, features_columns)

            while object_data_status is False:
                print("Sorry you don't choose a String object,\nSupport only on String object")
                ask_columns(optional_columns, type, df)
                break

        columns_to_filter_on = convert_string_to_list(columns_to_filter_on,",")

        target_columns = get_columns_to_gen(columns_to_filter_on, optional_columns)

        return features_columns , target_columns


def ask_for_identity_columns(optional_columns):
    identity_column = input("Please Choose the idenity columns if you don't have enter -1\n")
    if int(identity_column) == -1:
        return None

    while int(identity_column) not in optional_columns.keys():
        identity_column = input("Please choose a valid ID!!\n")

    identity_column = convert_string_to_list(optional_columns[int(identity_column)],",")
    return identity_column


def ask_for_file_size():
    size_count = int(rinput("Choose size (bytes):"))

    humanbytes(size_count)

    return size_count

def ask_for_output_file():

    # TODO ask for a new file or add to original file
    save_file = str(rinput("Enter full path to output file (name.csv):"))

    return save_file

def remove_id_from_target(target,id_column):
    target = returnNotMatches(target,id_column)
    return target[0]

def get_random_item(list):
    return random.choice(list)


def get_id_max_value(id_column,data):
    max_id = data.loc[data[id_column].idxmax()]
    return (int(max_id[id_column].values[0][0]))


def order_columns(df,df_orig):
    df_orig_cols = df_orig.columns.tolist()
    df = df[df_orig_cols]
    return df
