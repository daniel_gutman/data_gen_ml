#!/usr/bin/env python2.7

import gen_devops as gd
import argparse

# creates a parser object returns it
def create_parser():

    return argparse.ArgumentParser(argument_default=argparse.SUPPRESS)

# gets a parser object and adds an argument to it.
# default is that argument gets no value.
# in order to give a value to the argument, give a simple description string to 'value' arg.
# if the script can't work without that argument, give the must_arg - 'True'  
def add_argument_to_parser(parser, flag_name, flag_desc, value=True, must_arg=False, file_type=False, nargs=2):
    
    if value is True:
        parser.add_argument("--{}".format(flag_name),help=flag_desc,action="store_true",required=must_arg)
    elif value is False:
        parser.add_argument("--{}".format(flag_name),help=flag_desc,action="store_false",required=must_arg)
    else:
        if file_type:
            parser.add_argument("--{}".format(flag_name),help=flag_desc,metavar=value,required=must_arg,
                                type=argparse.FileType('r'), nargs=nargs)
        else:
            parser.add_argument("--{}".format(flag_name), help=flag_desc, metavar=value, required=must_arg,
                                nargs=nargs)

# gets a parser object and executes the parse args function on it with the command line arguments
# can get a list of arguments instead
# if the argument's list is empty, it will print the help board and will stop the script
# in case the arguments are parsed perfectly, it will return a dictionary with key as flag name and value as the value of the argument.
# no-value arguments will have the value 'False' as a value
def parser_parse_args(parser,args=gd.get_argv()[1:],terminate_no_args=True):
    
    if len(args) == 0:
        parser.print_help()
        if terminate_no_args:
            gd.stop_script("No args given")
    return vars(parser.parse_args(args))
