
import pandas as pd
import os
import subprocess
import pwd
import sys



def system3(cmd, wait=True):
    "run a system command and return the standard out, standard error and exit code"

    p = subprocess.Popen(['stdbuf', '-oL', '-e0','bash','-c', cmd],
                         stdout=subprocess.PIPE,
                         stderr=subprocess.PIPE)
    if wait:
        result,err = p.communicate()
        code = p.wait()
        return (result,err,code)
    else:
        return p


def server_ip():
    cmd = "hostname -I | cut -d' ' -f1"
    result, _, code = system3(cmd)

    if code == 0:
        return result.strip("\n")
    else:
        return "unknown server"

# adapting raw_input to file
def rinput(inp):
    return input("{}\n".format(inp))


def returnNotMatches(a, b):
    return [[x for x in a if x not in b], [x for x in b if x not in a]]



def get_columns_to_gen(columns_list,optional_columns):
    key_to_del = []
    columns_to_gen = []
    for index , values in optional_columns.items():
        for id in columns_list:
            if index == int(id):
                key_to_del.append(index)

    for key in key_to_del:
        del optional_columns[key]

    for index , values in optional_columns.items():
        columns_to_gen.append(values)

    return columns_to_gen

def get_choosen_columns(columns_list,optional_columns):
    choosen_columns_list = []
    for index , values in optional_columns.items():
        for choosen_id in columns_list:
            if index == int(choosen_id):
                choosen_columns_list.append(values)

    return choosen_columns_list

def convet_list_to_string(lt,delimiter):
    str = delimiter.join(lt)
    return str

def convert_string_to_list(str,delimiter):
    lt = str.split(delimiter)
    return lt

def save_epoch_data(dict_data,epoch,generated):
    dict_data[epoch] = generated
    return dict_data

def update_accuracy_to_train(dict_data,history):
    for epoch,acc in zip(dict_data,history):
        dict_data[acc] = dict_data[epoch]
        del dict_data[epoch]
    return dict_data

def get_max_accuracy_value(dict_data):
    max_accuracy = max(dict_data, key=float)
    return dict_data[max_accuracy]

def get_data(csv,delimiter,error_bad_lines):
    try:
        df = pd.read_csv(csv,delimiter=delimiter, error_bad_lines=error_bad_lines)
        return df
    except Exception as e:
        print(e.__cause__, e.args)

def write_csv(df,csv):
    df.to_csv(csv)


def checks_file_size(file):
    file.seek(0, 2)
    file_size = file.tell()
    return file_size



def humanbytes(B):
   'Return the given bytes as a human friendly KB, MB, GB, or TB string'
   B = float(B)
   KB = float(1024)
   MB = float(KB ** 2) # 1,048,576
   GB = float(KB ** 3) # 1,073,741,824
   TB = float(KB ** 4) # 1,099,511,627,776

   if B < KB:
      print('Generate size: {0} {1}'.format(B,'Bytes' if 0 == B > 1 else 'Byte'))
   elif KB <= B < MB:
      print('Generate size: {0:.2f} KB'.format(B/KB))
   elif MB <= B < GB:
      print('Generate size: {0:.2f} MB'.format(B/MB))
   elif GB <= B < TB:
      print('Generate size: {0:.2f} GB'.format(B/GB))
   elif TB <= B:
      print('Generate size: {0:.2f} TB'.format(B/TB))


def checks_delimiter(flags):
    if "delimiter" in flags:
        delimiter = rinput("Enter delimiter:")
    else:
        delimiter = ","
    return delimiter

def checks_bad_line_status(flags):
    if "error" in flags:
        return True
    else:
        return False
