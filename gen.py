#!/usr/bin/env python3

from utils import *
import gen_devops as gd
import filterd_data as fd
from tqdm import tqdm
import lstm as algo


def update_identity(new_data, max_id):
    new_data["id"] = [max_id]
    max_id = max_id + 1
    return max_id


def update_filters(new_data, df_filter, filters):
    for fil in filters:
        f = df_filter[fil].values
        new_data[fil] = [gd.get_random_item(f)]


def update_random(new_data, df_filter, random_choose):
    for rand in random_choose:
        r = df_filter[rand].values
        new_data[rand] = [gd.get_random_item(r)]


def update_LSTM(new_data, df_filter, learn, EPOCH, GPU):
    for ler in learn:
        l = df_filter[ler].values
        string_list = ["".join(item) for item in l.astype(str)]
        data = algo.gen_data(string_list, EPOCH, GPU)
        new_data[ler] = [data.new_data]


def genarate_data(save_file, size_count, filters, df_orig, id_column, learn, random_choose, max_id,
                  delimiter, CHUNK, EPOCH, GPU, file_size=0):

    new_data = {}

    print(max_id)

    pbar = tqdm(total=int(size_count))

    with open(save_file, 'w', encoding='utf-8') as file:
        while int(size_count) >= file_size:

            list_values_to_filter = []
            for f in filters:
                list_values_to_filter.append(fd.get_random_value_from_filter_data(df_orig, f))
            df_filter = fd.join_all_dataframes(list_values_to_filter, filters, df_orig, CHUNK)

            # df_filter_len = len(df_filter.index)

            if id_column:
                max_id = update_identity(new_data, max_id)

            update_filters(new_data, df_filter, filters)

            update_random(new_data, df_filter, random_choose)

            # lstm.py #
            update_LSTM(new_data, df_filter, learn, EPOCH, GPU)

            df = pd.DataFrame(data=new_data)

            df = gd.order_columns(df, df_orig)

            if file_size == 0:
                df.to_csv(file, header=True, sep=delimiter, index=False)
            else:
                df.to_csv(file, header=False, sep=delimiter, index=False)

            file_size = checks_file_size(file)
            pbar.update(file_size)

        pbar.close()
