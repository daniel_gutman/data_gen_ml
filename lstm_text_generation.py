#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from __future__ import print_function
from keras.callbacks import LambdaCallback
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from keras.optimizers import RMSprop
from keras.utils.data_utils import get_file
import numpy as np
import random
import sys
import utils
import time
import client

class LSTM_model(object):
    def __init__(self,data):
        self.data = data
        self.dict_data = {}

        print('corpus length:', len(self.data))

        self.chars = sorted(list(set(self.data)))
        print('total chars:', len(self.chars))
        self.char_indices = dict((c, i) for i, c in enumerate(self.chars))
        self.indices_char = dict((i, c) for i, c in enumerate(self.chars))

        # cut the text in semi-redundant sequences of maxlen characters
        self.maxlen = 80
        step = 3
        sentences = []
        next_chars = []
        for i in range(0, len(self.data) - self.maxlen, step):
            sentences.append(self.data[i: i + self.maxlen])
            next_chars.append(self.data[i + self.maxlen])
        print('nb sequences:', len(sentences))


        print('Vectorizaticd on...')
        x = np.zeros((len(sentences), self.maxlen, len(self.chars)), dtype=np.bool)
        y = np.zeros((len(sentences), len(self.chars)), dtype=np.bool)
        for i, sentence in enumerate(sentences):
            for t, char in enumerate(sentence):
                x[i, t, self.char_indices[char]] = 1
            y[i, self.char_indices[next_chars[i]]] = 1


        # build the model: a single LSTM
        print('Build model...')
        self.model = Sequential()
        print(self.maxlen)
        print(len(self.chars))
        self.model.add(LSTM(128, input_shape=(self.maxlen, len(self.chars))))
        self.model.add(Dense(len(self.chars), activation='softmax'))

        optimizer = RMSprop(lr=0.01)
        self.model.compile(loss='categorical_crossentropy', optimizer=optimizer,metrics=['accuracy'])
        print(self.model.summary())

        print_callback = LambdaCallback(on_epoch_end=on_epoch_end(self, epoch=10, _=None))

# this code from down page
        history = self.model.fit(x, y,
                            batch_size=128,
                            epochs=2,
                            callbacks=[print_callback])

        dict_data = utils.update_accuracy_to_train(self.dict_data, history.history['acc'])

        self.generated = utils.get_max_accuracy_value(dict_data)


        # utils.save_to_file(generated)

def sample(preds, temperature=1.0):
    # helper function to sample an index from a probability array
    preds = np.asarray(preds).astype('float64')
    preds = np.log(preds) / temperature
    exp_preds = np.exp(preds)
    preds = exp_preds / np.sum(exp_preds)
    probas = np.random.multinomial(1, preds, 1)
    return np.argmax(probas)


def on_epoch_end(self, epoch, _):
    # Function invoked at end of each epoch. Prints generated text.
    print()
    print('----- Generating text after Epoch: %d' % epoch)
    print("len(self.data):{}".format(len(self.data)))
    print("self.maxlen:{}".format(self.maxlen))
    print("len(self.data) - self.maxlen - 1:".format(len(self.data) - self.maxlen - 1))
    start_index = random.randint(0, len(self.data) - self.maxlen - 1)
    for diversity in [1.2]:
        print('----- diversity:', diversity)

        generated = ''
        sentence = self.data[start_index: start_index + self.maxlen]
        generated += sentence
        print('----- Generating with seed: "' + sentence + '"')
        # sys.stdout.write(generated)

        for i in range(800):
            x_pred = np.zeros((1, self.maxlen, len(self.chars)))
            for t, char in enumerate(sentence):
                x_pred[0, t, self.char_indices[char]] = 1.

            preds = self.model.predict(x_pred, verbose=0)[0]
            next_index = sample(preds, diversity)
            next_char = self.indices_char[next_index]

            generated += next_char
            sentence = sentence[1:] + next_char

            # sys.stdout.write(next_char)
            # sys.stdout.flush()

    utils.save_epoch_data(self.dict_data,epoch,generated)



