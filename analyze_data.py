#!/usr/bin/env python3

import numpy as np
from utils import *
from IPython.display import display

# def highlight_diff(data, color='yellow'):
#     attr = 'background-color: {}'.format(color)
#     other = data.xs('First', axis='columns', level=-1)
#     return pd.DataFrame(np.where(data.ne(other, level=0), attr, ''),
#                         index=data.index, columns=data.columns)

def read_files(flags):
    df_dict = {}
    delimiter = checks_delimiter(flags)
    error_bad_line = checks_bad_line_status(flags)
    for idx, file in enumerate(flags["describe data"]):
        df = get_data(file, delimiter, error_bad_line)
        df_dict[file] = df
    return df_dict


def describe(flags):
    df_dict = read_files(flags)

    for key, df in df_dict.items():
        print(key.name)
        print("-------------------------------------------------")
        display(df.describe(include='all'))
        print('\n')


def analyze(flags):
    print(flags)


def get_max_sting_object_value(flags):
    file = flags['get'][0].name
    delimiter = checks_delimiter(flags)
    error_bad_line = checks_bad_line_status(flags)
    df = get_data(file, delimiter, error_bad_line)

    df_object = df.select_dtypes(include=[object])
    measurer = np.vectorize(len)
    res = dict(zip(df_object, measurer(df_object.values.astype(str)).max(axis=0)))
    print(pd.DataFrame(res.items(),columns=['Columns Name','Max Length Value']))
