#!/usr/bin/env python3

from IPython.display import display, HTML
import pandas as pd
import numpy as np
from functools import reduce


# define our drop function
def drop_y(df):
    # list comprehension of the cols that end with '_y'
    to_drop = [x for x in df if x.endswith('_y')]
    df.drop(to_drop, axis=1, inplace=True)

# define our drop function
def drop_x(df):
    # list comprehension of the cols that end with '_x'
    to_drop = [x for x in df if x.endswith('_x')]
    df.drop(to_drop, axis=1, inplace=True)


def rename_x(df):
    for col in df:
        if col.endswith('_x'):
            df.rename(columns={col:col.rstrip('_y')}, inplace=True)



def get_random_value_from_filter_data(data,features):

    groupby_data = data.groupby(features, as_index=False).agg(np.random.choice)
    groupby_data = groupby_data.sample(n=1)

    return groupby_data[features].values


def join_all_dataframes(list_values_to_filter, filters, df, CHUNK):

    all_dataframe = []

    # print(list_values_to_filter[0][0])

    for value , column in zip(list_values_to_filter,filters):
        all_dataframe.append(df[df[column] == value[0]])

    if len(filters) > 1:
        df_final = reduce(lambda left,right: pd.merge(left,right ,on=filters, how='inner'), all_dataframe)
        drop_x(df_final)
    else:
        df_final = df.loc[df[filters[0]] == str(list_values_to_filter[0][0])]

    return df_final.head(CHUNK)


