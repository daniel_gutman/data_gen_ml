
import gen_devops as gd



def start_sqreamd(sqreamd_path, port, gpu, cluster, lic, config):
    sqreamd_cmd = "{} {} {} {} {} -config {}".format(sqreamd_path, cluster, gpu, port, lic, config)
    p = gd.start_bash_subprocess(sqreamd_cmd,catch_stds=True)
    comm = gd.comm_subprocess(p)
    return comm

def client_command(client_path, database, username, password, port, command, ddl_action):
    if ddl_action:
        client_cmd = "{} -d {} --user {} --pass {} --port {} -f {}".format(client_path, database, username, password,
                                                                             port, command)
    else:
        client_cmd = '{} -d {} --user {} --pass {} --port {} -c "{}"'.format(client_path, database, username, password,
                                                                                 port, command)
    result, error, code = gd.do_command(client_cmd)
    return result, error, code
